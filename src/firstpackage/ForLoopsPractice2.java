package firstpackage;

import java.util.Scanner;

public class ForLoopsPractice2 {

    //Ill print a multiplication table of a number entered by a user

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Please Enter A number : ");
        int number= scanner.nextInt();

        for (int i=0;i<=12;i++){
            System.out.println(number+ "* "+ i+" = "+(number*i));

        }
    }
}
