package firstpackage;

public class Notes {
    public static void main(String[] args) {
//        access specifiers
//        public, private, protected, default
//        Static: goes everywhere called classname.methodname
//        non-static: create object then objectname.methodname
//        void method: performs or print by creating object
//        return method: comes back with something so have to store in a String












        String day = "friday";

        // if(condition){ --if body starts
        // if body ends--}else if(condition) {-- else if body starts
        // else if body ends --}

        if (day.equalsIgnoreCase("Friday")) {
            System.out.println("I'll go pray on the mosque");
        } else if (day.equals("Thursday")) {
            System.out.println("I'll teach Java");
        } else {
            System.out.println("I'll work");
        }


        if (!day.equalsIgnoreCase("friday")) {
            System.out.println("do something");
        }

        int year = 2025;

        if (year == 2020) {
            System.out.println("Pandemic is here");
        } else if (year == 2021) {
            System.out.println("New varianat is here");
        } else {
            System.out.println("Pandemic is over");
        }

        if (year != 2023) {
            System.out.println("I'm gonna work");
        } else {
            System.out.println("year is 2023");

        }

        boolean snowTomorrow = false;
        if (snowTomorrow) { // if snowTomorrow is true
            System.out.println("i'm gonna watch netflix");
        }
        else {
            System.out.println("imma sleep");
        }


        if (!snowTomorrow) { // if snowTomorrow is false
            System.out.println("i'm gonna watch hulu");



        }
    }
}