package firstpackage;

public class ArrayForLoopsOtherWay {
    public static void main(String[] args) {
        String [] stateNames={"Ny","Ct","NJ","La","Mi"};
        for (int i=0;i<stateNames.length;i++){
            if (stateNames[i].equals("Ny")||stateNames[i].equals("Ct")||stateNames[i].equals("NJ")){
                System.out.println("tri state");
            }
            else{
                System.out.println("other");
            }
        }
    }
}
