package firstpackage;

public class TeamRealMadrid {

    static String nameOfTheClub;
    private final String manager;
    private final int totalPlayers;
    private String nameOfTrainer;
    static String nameofManager;
    static String rivalClub;
    static String bestPlayer;
    private String nameOfStriker;

    //
    public TeamRealMadrid(String manager, int totalPlayers) {
        this.manager = manager;
        this.totalPlayers = totalPlayers;
    }
    //1
    public static String getNameOfTheClub() {
        return nameOfTheClub + " is the name of the club";
    }
    //2
    public void coachNow() {
        System.out.println(manager + " is managing the team this season");
        System.out.println(totalPlayers + " is the number of the players");
    }
    //3
    public String getNameOfTheManager() {
        return nameofManager + " is the manager of the club";
    }
    //4
    public String getNameOfTheTrainer(){
        return nameOfTrainer+" is the trainer of the club";

    }

    //5
    public void setNameOfTrainer(String name){
        this.nameOfTrainer=name;
    }

    //6
    public static String getNameofRivalClub() {
        return rivalClub + " is the name of the rival  club";

    }

    //7
    public String getNameOfTheBestPlayer() {
        return bestPlayer + " is the best player of the club";
    }

    //8
    public String getNameOfTheStriker() {
        return nameOfStriker + " is the Striker of the club";
    }

    public void setNameOfStriker(String name) {
        this.nameOfStriker=name;
    }





}
