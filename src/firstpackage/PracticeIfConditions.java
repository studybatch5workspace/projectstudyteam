package firstpackage;

public class PracticeIfConditions {

    public static void main(String[] args) {
        String day="Friday";

        if (day.equalsIgnoreCase("Monday")){
            System.out.println("I got Class");
        }
        else if (day.equalsIgnoreCase("Thursday")){
            System.out.println("I got Class");
        }
        else {
            System.out.println("I got no class");
        }


        int number=8;

        if (number!=7){
            System.out.println("Its not ronaldos number");
        }
        else{
            System.out.println("Its Ronaldos number");
        }

        boolean whatHeSaid=true;
        if (whatHeSaid!=true){
            System.out.println("he lied");
        }
        else {
            System.out.println("He was telling the truth");
        }



    }
}
