package firstpackage;

public class Practice2Methods {

    // create 3 static variables and 3 non static variables (with value)
    // create 2 static method and 3 non static method

    // print the static variables into non static methods
    // print the static variables into static method

    // print the non static variables into non static methods
    // print the non static variable into static methods

    // finally call all the methods in the main method

    // create 3 static variables and 3 non static variables (with value)
    static String variable1="Toyota";
    static String variable2="Honda";
    static String variable3="Acura";


    String variable4="Mercedes";
    String variable5="BMW";
    String variable6="Nissan";

    // create 2 static method
    public static void itsAStaticMethod1(){
        System.out.println(variable1);
        System.out.println(variable2);
        System.out.println(variable3);

        Practice2Methods practice2Methods=new Practice2Methods();
        System.out.println(practice2Methods.variable4);
        System.out.println(practice2Methods.variable5);
        System.out.println(practice2Methods.variable6);


    }
    public static void itsAStaticMethod2(){
        System.out.println(variable1);
        System.out.println(variable2);
        System.out.println(variable3);

        Practice2Methods practice2Methods=new Practice2Methods();
        System.out.println(practice2Methods.variable4);
        System.out.println(practice2Methods.variable5);
        System.out.println(practice2Methods.variable6);
    }
//     3 non static method
    public void itsANonStaticMethod1(){
        System.out.println(variable1);
        System.out.println(variable2);
        System.out.println(variable3);

        System.out.println(variable4);
        System.out.println(variable5);
        System.out.println(variable6);
    }
    public void itsANonStaticMethod2(){
        System.out.println(variable1);
        System.out.println(variable2);
        System.out.println(variable3);


        System.out.println(variable4);
        System.out.println(variable5);
        System.out.println(variable6);
    }
    public void itsANonStaticMethod3(){
        System.out.println(variable1);
        System.out.println(variable2);
        System.out.println(variable3);


        System.out.println(variable4);
        System.out.println(variable5);
        System.out.println(variable6);
    }

    public static void main(String[] args) {
        itsAStaticMethod1();
        itsAStaticMethod2();

        Practice2Methods practice2Methods=new Practice2Methods();
        practice2Methods.itsANonStaticMethod1();
        practice2Methods.itsANonStaticMethod2();
        practice2Methods.itsANonStaticMethod3();

    }
}
