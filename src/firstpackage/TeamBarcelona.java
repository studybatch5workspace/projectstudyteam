package firstpackage;

public class TeamBarcelona {

    static String nameOfTheClub;
    private final String manager;
    private final int totalPlayers;
    private String nameOfTrainer;
    static String nameOfManager;
    static String rivalClub;
    static String bestPlayer;
    private String nameOfStriker;
    private String nameOfPresident;
    private String nameOfBottle;
    private String nameOfMusic;


    //0
    protected TeamBarcelona(String manager, int totalPlayers) {
        this.manager = manager;
        this.totalPlayers = totalPlayers;
    }
    //1
    public static String getNameOfTheClub() {
        return nameOfTheClub + " is the name of the club";
    }
    //2
    public void coachNow() {
        System.out.println(manager + " is managing the team this season");
        System.out.println(totalPlayers + " is the number of the players");
    }
    //3
    public String getNameOfTheManager() {
        return nameOfManager + " is the manager of the club";
    }
    //4
    public String getNameOfTheTrainer(){
        return nameOfTrainer+" is the trainer of the club";

    }
    public void setNameOfTheTrainer(String name){
        this.nameOfTrainer=name;
    }



    //5
    public static String getNameofRivalClub() {
        return rivalClub + " is the name of the rival  club";

    }

    //6
    public String getNameOfTheBestPlayer() {
        return bestPlayer + " is the best player of the club";
    }

    //7
    public String getNameOfTheStriker() {
        return nameOfStriker + " is the Striker of the club";
    }

    public void setNameOfStriker(String name){
        this.nameOfStriker=name;
    }


//8
    public String getNameOfPresident(){
        return nameOfPresident+" is the president of the club";

    }
    public void setNameOfPresident(String name){
        this.nameOfPresident=name;
    }


//9
    public String getNameOfBottle(){
        return nameOfBottle+" is the bottle of the club";

    }
    public void setNameOfTheBottle(String name){
        this.nameOfBottle=name;
    }


//10
    public String getNameOfMusic(){
        return nameOfMusic+" is the music of the club";

    }
    public void setNameOfMusic(String name){
        this.nameOfMusic=name;
    }


}
