package firstpackage;

public class ForLoopsPractice {
    public static void main(String[] args) {
//        I'll use for loops to print numbers 0-10 and -10 to 0

        for (int i=0;i<=10;i++){
            System.out.println(i);
        }
        for (int j=-10;j<=0;j++){
            System.out.println(j);
        }
    }
}


