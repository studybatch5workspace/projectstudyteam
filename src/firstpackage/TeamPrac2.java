package firstpackage;

public class TeamPrac2 {
    public static void main(String[] args) {


        String[] stateNames = {"NY", "CA", "LA", "NJ", "VA"};


        for (int i = 0; i < stateNames.length; i++) {
            if (stateNames[i].equals("NY") || stateNames[i].equals("NJ") || stateNames[i].equals("CT")) {
                System.out.println(stateNames[i] + " : Tristate");
            } else {
                System.out.println(stateNames[i] + " : Other");
            }
        }


    }
}
