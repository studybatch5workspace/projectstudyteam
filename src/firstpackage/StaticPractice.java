package firstpackage;

public class StaticPractice {

    // create 3 static variables and 3 non static variables (with value)
    // create 2 static method and 3 non static method
    // print the static variables into non static methods
    // print the static variables into static method
    // print the non static variables into non static methods
    // print the non static variable into static methods
    // finally call all the methods in the main method


    static String varaiable1 = "WhiteBottle";
    static String varaiable2 = "BlackBottle";
    static String varaiable3 = "YellowBottle";
    String varaiable4 = "BlueBottle";
    String varaiable5 = "PinkBottle";
    String varaiable6 = "OrangeBottle";

    public static void printStaticMethod1() {
        // print the static variables into static method
        System.out.println(varaiable1);
        System.out.println(varaiable2);
        System.out.println(varaiable3);

        // print the non static variable into static methods
        StaticPractice staticPractice = new StaticPractice();
        System.out.println(staticPractice.varaiable4);
        System.out.println(staticPractice.varaiable5);
        System.out.println(staticPractice.varaiable6);
    }

    public static void printStaticMethod2() {
        // print the static variables into static method
        System.out.println(varaiable1);
        System.out.println(varaiable2);
        System.out.println(varaiable3);

        // print the non static variable into static methods
        StaticPractice staticPractice = new StaticPractice();
        System.out.println(staticPractice.varaiable4);
        System.out.println(staticPractice.varaiable5);
        System.out.println(staticPractice.varaiable6);
    }

    public void printStaticMethod3() {
        // print the static variables into non static methods
        System.out.println(varaiable1);
        System.out.println(varaiable2);
        System.out.println(varaiable3);

        // print the non static variables into non static methods
        System.out.println(varaiable4);
        System.out.println(varaiable5);
        System.out.println(varaiable6);

    }

    public void printStaticMethod4() {
        // print the static variables into non static methods
        System.out.println(varaiable1);
        System.out.println(varaiable2);
        System.out.println(varaiable3);

        // print the non static variables into non static methods
        System.out.println(varaiable4);
        System.out.println(varaiable5);
        System.out.println(varaiable6);
    }

    public void printStaticMethod5() {
        // print the static variables into non static methods
        System.out.println(varaiable1);
        System.out.println(varaiable2);
        System.out.println(varaiable3);

        // print the non static variables into non static methods
        System.out.println(varaiable4);
        System.out.println(varaiable5);
        System.out.println(varaiable6);
    }

    public static void main(String[] args) {
        // finally call all the methods in the main method
        printStaticMethod1();
        printStaticMethod2();

//   Object to call non-static Methods
        StaticPractice staticPractice = new StaticPractice();
        staticPractice.printStaticMethod3();
        staticPractice.printStaticMethod4();
        staticPractice.printStaticMethod5();


    }

}
