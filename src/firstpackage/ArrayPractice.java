package firstpackage;

public class ArrayPractice {


        public static void main(String[] args) {
            String[] names = {"MD", "Sub", "Jawad", "Zann", "Who"};

//        System.out.println(names[0]);
//        System.out.println(names[1]);
//        System.out.println(names[2]);
//        System.out.println(names[3]);


            names[2] = "Opu";
            names[1] = "Samiyan";
            System.out.println(names.length);

            for (int i = 0; i < names.length; i++) {
                System.out.println(names[i]);
            }

            System.out.println("***************************");
            // Store 5 state names
            // out of them for the state name being NY or NJ or CT print "Tristate" and for the rest print "Other"

            String[] stateNames = {"NY", "CA", "LA", "NJ", "VA"};


            for (int i = 0; i < stateNames.length; i++) {
                if (stateNames[i].equals("NY") || stateNames[i].equals("NJ") || stateNames[i].equals("CT")) {
                    System.out.println(stateNames[i] + " : Tristate");
                } else {
                    System.out.println(stateNames[i] + " : Other");
                }
            }


            System.out.println("***************************");


            String[] locations = new String[5];
            int[] zipCodes = new int[5];

            zipCodes[0] = 11374;
            zipCodes[1] = 11375;

            // 50 lines of codes

            zipCodes[2] = 11376;
            zipCodes[3] = 11377;
            zipCodes[4] = 11378;

            for (int i = 0; i < zipCodes.length; i++) {
                System.out.println(zipCodes[i]);
            }

            Object[] everyThing = new Object[3];
            everyThing[0] = 11374;
            everyThing[1] = "NY";
            everyThing[2] = true;


            // multi dimensional array

            String[][] stateAndArea = new String[5][2];

            stateAndArea[2][0] = "NY";
            stateAndArea[2][1] = "RegoPark";


            stateAndArea[1][0] = "NJ";
            stateAndArea[1][1] = "Hoboken";

            System.out.println(stateAndArea[2][0]);
            System.out.println(stateAndArea[2][1]);

            System.out.println("***************************");


            Object[][] stateAndZip = {{"NY", 11374, true}, {"NJ", 11333}, {}};


            System.out.println(stateAndZip[0][2]);


        }}
