package myownpackage;
//I will push  bunch of ArrayLists Inside HashMap and call and print them.
import java.util.ArrayList;
import java.util.HashMap;

public class ArrayListInsideHashMap {
    public static void main(String[] args) {
        HashMap<String, ArrayList<String>> typeOfFood = new HashMap<>();
        ArrayList<String> typeOfSodas = new ArrayList<>();
        typeOfSodas.add("Ginger Ale");
        typeOfSodas.add("Mountain Dew");
        typeOfSodas.add("7Up");
        typeOfSodas.add("Coke");
        typeOfSodas.add("Pepsi");
        typeOfSodas.add("Diet Coke");
        typeOfSodas.add("Diet Pepsi");
        typeOfSodas.add("Crush");


        ArrayList<String> typeOfBasketballTeams = new ArrayList<>();
        typeOfBasketballTeams.add("Lakers");
        typeOfBasketballTeams.add("GSW");
        typeOfBasketballTeams.add("Cavs");
        typeOfBasketballTeams.add("Grizzlies");
        typeOfBasketballTeams.add("Clippers");
        typeOfBasketballTeams.add("Bucks");
        typeOfBasketballTeams.add("Mavericks");


        ArrayList<String> differentBrands = new ArrayList<>();
        differentBrands.add("Gucci");
        differentBrands.add("LuisVuitton");
        differentBrands.add("Armani");
        differentBrands.add("PRL");
        differentBrands.add("Nautica");
        differentBrands.add("Nike");
        differentBrands.add("Adidas");


        ArrayList<String> differentLanguages = new ArrayList<>();
        differentLanguages.add("English");
        differentLanguages.add("Bengali");
        differentLanguages.add("Spanish");
        differentLanguages.add("Hindi");
        differentLanguages.add("Urdu");
        differentLanguages.add("French");

        // another way to call and print
//        ArrayList<String> oo = typeOfFood.get("countries");// another way to call and print
//        System.out.println(oo.get(0));


        typeOfFood.put("List of Sodas", typeOfSodas);
        System.out.println(typeOfFood.get("List of Sodas"));


        typeOfFood.put("Basketball Teams", typeOfBasketballTeams);
        System.out.println(typeOfFood.get("Basketball Teams"));

        typeOfFood.put("Most Famous Brands", differentBrands);
        System.out.println(typeOfFood.get("Most Famous Brands"));

        typeOfFood.put("Famous Languages", differentLanguages);
        System.out.println(typeOfFood.get("Famous Languages"));


    }
}
